/*--------------------------------------------------------------------------------------------------------------------*\
| IEEE SOUTHEASTCON infrared receiver module                                                                           |
| Based on the example file IRrecvDumpV2.ino. It uses a modified version of the IRremote library to cut out protocols  |
| we will not be using (that is, all of them save for the IEEE one) for the competition. Do NOT use the version of the |
| library you can find in Arduino's library browser. Instead, copy over the IRremote folder to:                        |
|  Windows: C:\Users\username\Arduino\libraries\                                                                       |
| OSX/*nix: /home/Arduino/libraries/* (not sure, I haven't done Arduino work on my Arch install in over a year)        |
|                                                                                                                      |
| The IRremote folder should be a subdirectory of libraries only; it should not be a subdirectory of another folder.   |
|                                                                                                                      |
| Please refer to the rules if you wish to confirm the timing definitions included.                                    |
\*--------------------------------------------------------------------------------------------------------------------*/

// ----- /!\ ----- efines -- PART OF MODULE. ADD TO FINAL'S INCLUDES!!
#include <IRremote.h>

#define ROUTE_C 4
#define ROUTE_B 2
#define ROUTE_A 1
#define debounceTime 30


const PROGMEM uint8_t recv_pin = 30; //Pin no.
IRrecv ir_rx(recv_pin);             //Constructs IRrecv obj. to pin specified
uint8_t  routeNo = 0;                //Rest of code will reference routeNo.
decode_results results;            //Somewhere to store the results; if we get stretched for memory I'll
                                     //take the time to 
// ----- /!\ ----- END INCLUDES TO ADD TO FINAL

double lastPressed=0;
void taskTwo(uint8_t firstDecision);

  volatile int taskNumber = 0;
  void changeTask();
  void setup() {
  Serial.begin(9600);
  //setting


  attachInterrupt(digitalPinToInterrupt(2), changeTask, FALLING);
  attachInterrupt(digitalPinToInterrupt(19), changeTask, FALLING);
    Serial.begin(9600);
    Serial.println("RDY");
}
 
void  loop ()
{
  taskOne();
}

void changeTask() {
  if (millis() - lastPressed > debounceTime) {
    lastPressed = millis();
    taskNumber++;
}}

void taskOne(){
  // ----- /!\ ----- [Module: IR] BEGIN COPY INTO TASK 0
  ir_rx.enableIRIn();   // Start the receiver
  routeNo = getIRcmd(); // Uses best out of five "voting" for error correction
  // ----- /!\ ----- [Module:IR] END COPY INTO TASK 0
  Serial.print("Recv code: ");
  Serial.println(routeNo);
  delay(4000);
}

// ----- /!\ ----- [Module: IR] COPY THESE FUNCTIONS INTO FINAL
uint8_t getIRcmd()
{
    uint8_t retVal = 0;                         //Will remain zero if no commands recv; failsafe route.
    uint8_t inputBuf;                           //Temporary storage indicating the path received.
    uint8_t numCounts[8] = {0};                 //Creates 8-wide array, num of received commands, idx=cmd

    //Decodes five signals from IR and uses them to tally "votes" for which command was received.
    for(uint8_t i=0;i<5;i++) {
        inputBuf = readIR();                    //Gets last byte received over IR.
        if(i<4) ir_rx.resume();                 //Listen only five times; does not resume on final run
        if(inputBuf < 8) numCounts[inputBuf]++; //Only eight(0-7) states; others are received in error
                                                //Uses inputBuf to select which slot to inc.
    }

    //Runs through the eight commands, updating retVal when a higher value is found.
    for(uint8_t i=1;i<8;i++){
       if(numCounts[retVal] < numCounts[i]) retVal = i;
    }

    //Off to see the wizard...
    return retVal;
}

uint8_t readIR()
{
    uint32_t startMillis = millis() ; // Time for 1s per attempt

    while(true){                             // Busywait for a command for 1000ms
        if (ir_rx.decode(&results)) {        // Did we decode a command?
            if(results.decode_type == IEEE){ // No, really; did we? Rejects garbled cmds.
              return results.value;     
            }
            return 128;                      // FAIL: command was garbled.
        }
        if (millis()-startMillis > 5000) {   // Have we waited for too long?
          return 127;                        // FAIL: waited over 1s to decode.
        }
    }
}
// ----- /!\ ----- [Module: IR] END MATERIAL TO COPY
