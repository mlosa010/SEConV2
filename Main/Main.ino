#include <IRremote.h>
#include "motorController.h"
#include "sensorArray.h"


sensorArray mySensors;
#define RLS 19
#define LLS 2
#define debounceTime 250


#define ROUTE_C 4
#define ROUTE_B 2
#define ROUTE_A 1


const PROGMEM uint8_t recv_pin = 30; //Pin no.
IRrecv ir_rx(recv_pin);             //Constructs IRrecv obj. to pin specified
uint8_t  routeNo = 0;                //Rest of code will reference routeNo.
decode_results results;

//Wheel Turner
const int stepPin = 9;
const int dirPin = 33;
const int enPin = 34;



//---------------construction of individual motors(PWM,digital, digital)---------------

int speed1 = 176;
int speed2 = 188;
int speed3 = 121;
int speed4 = 191;

Motor motor1(7, 26, 22, speed1 / 2 ); //top port even 100
Motor motor2(8, 27, 50, speed2 / 2 ); //top starbird even 125
Motor motor3(4, 28, 24, speed3 / 2 ); //bottom port even 70
Motor motor4(5, 29, 25, speed4 / 2 ); //bottom starbird even 129

//----------------------construction of motor controllers---------------------
motorController myController(motor1, motor2, motor3, motor4);

// volatile data types denote variables changed within interrupts
volatile int taskNumber = 4;
void changeTask();
void taskTwo(uint8_t firstDecision);
void taskThree(uint8_t firstDecision);
void taskFour();
void getSensors();

void setup() {
  Serial.begin(9600);
  //setting
  pinMode(RLS, INPUT_PULLUP);
  pinMode(LLS, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(2), changeTask, FALLING);
  attachInterrupt(digitalPinToInterrupt(19), changeTask, FALLING);
  mySensors.sensorSetUp();

  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);

  pinMode(enPin, OUTPUT);
  digitalWrite(enPin, LOW);

}

void loop() {
  Serial.println(taskNumber);
  switch (taskNumber) {
    case 0: // IR Code
      myController.haults();
      taskOne();
      taskNumber++;
      break;
    case 1: // beginning of the stage to touch arcade buttons
      taskTwo(routeNo & ROUTE_A);
      break;
    case 2: // after arcade buttons are touch go back to middle
      taskThree(routeNo & ROUTE_A);
      break;
    case 3: // going down the ramp
      taskFour();
      break;
    case 4: // moving forwards to align with pressure plates
      taskFive();
      break;
    case 5: // hit the designated pressure plate
      taskSix(routeNo & ROUTE_B);
      break;
    case 6: // move forward to the wall of captain wheels
      taskSeven();
      break;
    case 7: // move to captain wheels
      taskEight(routeNO & ROUTE_B);
      break;
    case 8: // Spin captain wheels
      taskNine();
      break;
    default:
      myController.haults();
      printTask();
  }

}



void taskOne() {
  // ----- /!\ ----- [Module: IR] BEGIN COPY INTO TASK 0
  ir_rx.enableIRIn();   // Start the receiver
  routeNo = getIRcmd(); // Uses best out of five "voting" for error correction
  // ----- /!\ ----- [Module:IR] END COPY INTO TASK 0
  Serial.print("Recv code: ");
  Serial.println(routeNo);
}

//----------------------the interrupt service routine----------
void changeTask() {
  unsigned long x = millis();
  if (millis() - x < 60) {
    taskNumber++;
  }

}


void taskTwo(int firstBit) {
  if (firstBit) {
    while (taskNumber == 1) {
      if (mySensors.getSensor1() < 21) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 27) {
        myController.backwards();
      } else {
        myController.left();
      }
    }
  }
  else {
    while (taskNumber == 1) {
      if (mySensors.getSensor1() < 21 ) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 27) {
      }
      else {
        myController.right();
      }
    }
  }
}

void taskThree(int firstBit) {
  if (firstBit) {
    while (mySensors.getSensor2() < 50 && taskNumber == 2) {
      if (mySensors.getSensor1() < 21 ) {
        myController.forwards();
        getSensors();
      }
      else if (mySensors.getSensor1() > 27) {
        myController.backwards();
        getSensors();
      } else {
        myController.right();
        getSensors();
      }
    }
  }
  else {
    while (mySensors.getSensor4() < 50 && taskNumber == 2) {
      if (mySensors.getSensor1() < 21 ) {
        myController.forwards();
        getSensors();
      }
      else if (mySensors.getSensor1() > 27) {
        myController.backwards();
        getSensors();
      }
      else {
        myController.left();
        getSensors();
      }
    }
  }
  taskNumber++;
}

void taskFour() {
  unsigned long current = millis();
  unsigned long previous = current;
  unsigned long interval = 2000;

  //  unsigned long current = millis();
  while (taskNumber == 3) {
    if (current - previous < interval) {
      myController.backwards();
    }
    else {
      myController.haults();
      taskNumber++;
    }
    current = millis();
  }
}

void taskFive() {
  while (taskNumber == 4) {
    if (mySensors.getSensor3() >= 30) {
      myController.forwards();
    }
    else {
      taskNumber++;
    }
  }
}

void taskSix(int secondDecision) {
  while (taskNumber == 5) {
    boolean firstTask = 1;
    if (secondDecision) {
      if (mySensors.getSensor2 <= 18 && firstTask) {
        myController.right();
      }
      else {
        firstTask = 0;
        if (mySensor.getSensor2 <= 23) {
          myController.left();
        }
        else {
          myController.haults();
          taskNumber++;
        }
      }
    }
    else {
      if (mySensors.getSensor4() <= 18 && firstTask) {
        myController.left();
      }
      else {
        firstTask = 0;
        if (mySensor.getSensor4() <= 23) {
          myController.right();
        }
        else {
          myController.haults();
          taskNumber++;
        }
      }
    }
  }
}

void taskSeven() {
  while (taskNumber == 6) {
    if (mySensors.getSensor3() > 6) {
      myController.forwards();
    }
    else {
      taskNumber++;
    }
  }

}

void taskEight(int secondDecision) {
  if (secondDecision) {
    while (taskNumber == 7) {
      if (mySensors.getSensor4() > 46) {
        myController.right();
      }
      else {
        taskNumber++;
      }
    }
  }
  else {
    while (taskNumber == 7) {
      if (mySensors.getSensor2() > 46) {
        myController.left();
      }
      else {
        taskNumber++;
      }
    }
  }
}


void taskNine() {
  if (mySensors.getSensor3MM() >= 25) {
    myController.forwards();
  }
  else {
    myController.haults();
    delay(200);
    digitalWrite(dirPin, HIGH); //Changes the rotations direction
    // Makes 5 full cloclwisecycle rotations as in the competition
    for (int x = 0; x < 1000; x++) {
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(500);
      digitalWrite(stepPin, LOW);
      delayMicroseconds(500);
      taskNumber++;
    }

  }
}




// ----- /!\ ----- [Module: IR] COPY THESE FUNCTIONS INTO FINAL
uint8_t getIRcmd()
{
  uint8_t retVal = 0;                         //Will remain zero if no commands recv; failsafe route.
  uint8_t inputBuf;                           //Temporary storage indicating the path received.
  uint8_t numCounts[8] = {0};                 //Creates 8-wide array, num of received commands, idx=cmd

  //Decodes five signals from IR and uses them to tally "votes" for which command was received.
  for (uint8_t i = 0; i < 5; i++) {
    inputBuf = readIR();                    //Gets last byte received over IR.
    if (i < 4) ir_rx.resume();              //Listen only five times; does not resume on final run
    if (inputBuf < 8) numCounts[inputBuf]++; //Only eight(0-7) states; others are received in error
    //Uses inputBuf to select which slot to inc.
  }

  //Runs through the eight commands, updating retVal when a higher value is found.
  for (uint8_t i = 1; i < 8; i++) {
    if (numCounts[retVal] < numCounts[i]) retVal = i;
  }

  //Off to see the wizard...
  return retVal;
}

uint8_t readIR() {
  uint32_t startMillis = millis() ; // Time for 1s per attempt

  while (true) {                           // Busywait for a command for 1000ms
    if (ir_rx.decode(&results)) {        // Did we decode a command?
      if (results.decode_type == IEEE) { // No, really; did we? Rejects garbled cmds.
        return results.value;
      }
      return 128;                      // FAIL: command was garbled.
    }
    if (millis() - startMillis > 90000) { // Have we waited for too long?
      return 127;                        // FAIL: waited over 1s to decode.
    }
  }
}

void getSensors() {
  Serial.print("Back: ");
  Serial.println(mySensors.getSensor1()); //back
  Serial.print("Right: ");
  Serial.println(mySensors.getSensor2()); //right
  Serial.print("Front: ");
  Serial.println(mySensors.getSensor3()); //front
  Serial.print("Left: ");
  Serial.println(mySensors.getSensor4()); //left
  Serial.println();
}

void printTask() {
  Serial.println(taskNumber);
}

