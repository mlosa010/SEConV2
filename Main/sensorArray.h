#ifndef sensorArray_h
#include "Arduino.h"
#include <VL53L0X.h>



class sensorArray{
  public:
  sensorArray();
  sensorSetUp();
  int getSensor1();
  int getSensor2();
  int getSensor3();
  int getSensor4();
  int getSensor3MM();
  int getAll();
  VL53L0X sensor;
  VL53L0X sensor2;
  VL53L0X sensor3;
  VL53L0X sensor4;

  
  private:

};
#endif

